#include "src/headers/Camera.h"
#include "src/headers/Globals.h"
#include "src/headers/logger.h"
#include <GLFW/glfw3.h>
#include <algorithm>

extern int SCREEN_WIDTH;
extern int SCREEN_HEIGHT;

extern GLFWwindow* g_window;

Camera::Camera(glm::vec3 pos, glm::vec3 viewDir, glm::vec3 upDir, CameraMode newMode)
{
    transform.position = pos;
    lookatDirection = viewDir;
    upDirection = upDir;
    mode = newMode;

    rotationAngle = 45.0f;
    rotationSpeed = 0.5f;
    rotationDirection = glm::vec3(0, 1, 0);
    lookatPosition = glm::vec3(0, 0, 0);
    followDistance = glm::vec3(0, 0, 1);

    view = glm::mat4(1.0f);
    projection = glm::perspective(glm::radians(g_FOV), float(SCREEN_WIDTH) / float(SCREEN_HEIGHT), 0.1f, g_viewDistance);
}

void Camera::update()
{
    switch (mode)
    {
    case freeCam:
        view = glm::lookAt(transform.position, transform.position + lookatDirection, upDirection);
        view = view * transform.rotation;
        break;

    case staticCam:
        view = glm::lookAt(transform.position, lookatPosition, upDirection);
        break;

    case followCam:
        transform.position = target->transform.position + glm::vec3(target->transform.rotation * glm::vec4(followDistance, 1.0f));
        view = glm::lookAt(transform.position, target->transform.position, upDirection);
        break;

    case trackingCam:
        view = glm::lookAt(transform.position, target->transform.position, upDirection);
        break;

    case orbitTargetCam:
        // Orbit gameobject
        transform.rotation = glm::rotate(glm::mat4(1.0f), float(glm::radians(rotationAngle) * g_deltatime * rotationSpeed), rotationDirection);

        followDistance = transform.rotation * glm::vec4(followDistance, 1.0);
        transform.position = target->transform.position + followDistance;
        view = glm::lookAt(transform.position, target->transform.position, upDirection);
        break;

    case orbitPositionCam:
        transform.rotation = glm::rotate(glm::mat4(1.0f), float(glm::radians(rotationAngle) * g_deltatime * rotationSpeed), rotationDirection);

        followDistance = transform.rotation * glm::vec4(followDistance, 1.0);
        transform.position = lookatPosition + followDistance;
        view = glm::lookAt(transform.position, lookatPosition, upDirection);
        break;
    case gliderCam:
        transform.position = target->transform.position + glm::vec3(target->transform.rotation * glm::vec4(glm::vec3(9, 2, 0), 1.0f));
        view = glm::lookAt(transform.position, target->transform.position, upDirection);
        break;
    }

}

void Camera::mouseInput(double xpos, double ypos)
{
    if (mode == freeCam)
    {
        if (xpos > 0)
        {
            transform.rotation = glm::rotate(glm::mat4(1.0), float(glm::radians(rotationAngle) * g_deltatime * rotationSpeed * 4), glm::vec3(0, -1, 0));
            lookatDirection = transform.rotation * glm::vec4(lookatDirection, 1.0);
        }
        else if (xpos < 0)
        {
            transform.rotation = glm::rotate(glm::mat4(1.0), float(glm::radians(rotationAngle) * g_deltatime * rotationSpeed * 4), glm::vec3(0, 1, 0));
            lookatDirection = transform.rotation * glm::vec4(lookatDirection, 1.0);
        }

        if (ypos > 0)
        {
            transform.rotation = glm::rotate(glm::mat4(1.0), float(glm::radians(rotationAngle) * g_deltatime * rotationSpeed * 4), glm::vec3((lookatDirection.z >= 0)? 1 : -1, 0, 0));
            lookatDirection = transform.rotation * glm::vec4(lookatDirection, 1.0);
        }
        else if (ypos < 0)
        {
            transform.rotation = glm::rotate(glm::mat4(1.0), float(glm::radians(rotationAngle) * g_deltatime * rotationSpeed * 4), glm::vec3((lookatDirection.z >= 0)? -1 : 1, 0, 0));
            lookatDirection = transform.rotation * glm::vec4(lookatDirection, 1.0);
        }
    }
}

void Camera::input(int key, int scancode, int action, int mods)
{
    if (key == GLFW_KEY_C && action == GLFW_PRESS)
    {
        mode = static_cast<CameraMode>(int(mode) + 1);
        if (int(mode) > 5)
        {
            mode = freeCam;
            transform.position = glm::vec3(0, 0, 5);
            transform.rotation = glm::mat4(1.0f);
        }
        LOG_DEBUG("Cam mode: %d", int(mode));
    }
    if (key == GLFW_KEY_SLASH  && action == GLFW_PRESS) //Set lock on glider.
    {
        if (mode != gliderCam)
        {
            mode = gliderCam;
        }
        else
        {
            mode = freeCam;
            transform.position = glm::vec3(0, 1, 8);
            transform.rotation = glm::mat4(1.0f);
        }
    }
    if (key == GLFW_KEY_M) //Set lock on glider.
    {
        g_FOV--;
        if (g_FOV < 10)
            g_FOV = 10;
        projection = glm::perspective(glm::radians(g_FOV), float(SCREEN_WIDTH) / float(SCREEN_HEIGHT), 0.1f, g_viewDistance);
    }

    if (key == GLFW_KEY_N) //Set lock on glider.
    {
        g_FOV++;
        if (g_FOV > 150)
            g_FOV = 150;
        projection = glm::perspective(glm::radians(g_FOV), float(SCREEN_WIDTH) / float(SCREEN_HEIGHT), 0.1f, g_viewDistance);
    }

    if (mode == freeCam)
    {

        //Forward
        if (key == GLFW_KEY_I)
        {
            transform.position += lookatDirection * 2.0f;       //lookatDirection gives direction based on current local rotation.
        }
        //Backward
        if (key == GLFW_KEY_K)
        {
            transform.position -= lookatDirection * 2.0f;
        }
        //Right
        if (key == GLFW_KEY_L)
        {
            //I use forward direction and rotate a temp of it to right/left/up/down for getting right direction of camera movement.
            glm::vec3 tempDirection = glm::cross(lookatDirection, glm::vec3(0, 2.0f, 0));

            transform.position += tempDirection;
        }
        //Left
        if (key == GLFW_KEY_J)
        {
            glm::vec3 tempDirection = glm::cross(lookatDirection, glm::vec3(0, -2.0f, 0));

            transform.position += tempDirection;
        }

        //Up and down are a little weird, but works somewhat well.
        //UP
        if (key == GLFW_KEY_Y)
        {
            glm::vec3 tempDirection = glm::cross(lookatDirection, glm::vec3((lookatDirection.z >= 0)? 2 : -2, 0, 0));

            transform.position += tempDirection;
        }
        //Down
        if (key == GLFW_KEY_H)
        {
            glm::vec3 tempDirection = glm::cross(lookatDirection, glm::vec3((lookatDirection.z >= 0)? -2 : 2, 0, 0));

            transform.position += tempDirection;
        }
    }
}

void Camera::rotate(glm::vec3 dir)
{
    transform.rotation = glm::rotate(glm::mat4(1.0), float(glm::radians(rotationAngle) * g_deltatime * rotationSpeed * 4), dir);
    lookatDirection = transform.rotation * glm::vec4(lookatDirection, 1.0);
}

void Camera::tilt(glm::vec3 dir)
{
    transform.rotation = glm::rotate(glm::mat4(1.0), float(glm::radians(rotationAngle) * g_deltatime * rotationSpeed * 4), dir);
    upDirection = transform.rotation * glm::vec4(upDirection, 1.0);
}

void Camera::setCamMode(CameraMode newMode)
{
    mode = newMode;
}

void Camera::setFollowDistance(glm::vec3 newDistance)
{
    followDistance = newDistance;
}

void Camera::setUpDirection(glm::vec3 newDirection)
{
    upDirection = newDirection;
}

void Camera::setLookatDirection(glm::vec3 newDirection)
{
    lookatDirection = newDirection;
}

void Camera::setLookatPosition(glm::vec3 newPosition)
{
    lookatPosition = newPosition;
}

void Camera::setTarget(GameObject* newTarget)
{
    target = newTarget;
}

void Camera::setRotationDirection(glm::vec3 newDirection)
{
    rotationDirection = newDirection;
}

void Camera::setRotationAngle(float angle)
{
    rotationAngle = angle;
}

void Camera::setRotationSpeed(float speed)
{
    rotationSpeed = speed;
}

void Camera::updateProjection(int width, int height)
{
    projection = glm::perspective(glm::radians(g_FOV), float(width) / float(height), 0.1f, g_viewDistance);
}

glm::vec3 Camera::getFollowDistance()
{
    return followDistance;
}

glm::vec3 Camera::getUpDirection()
{
    return upDirection;
}

glm::vec3 Camera::getLookatDirection()
{
    return lookatDirection;
}

glm::vec3 Camera::getLookatPosition()
{
    return lookatPosition;
}

glm::mat4 Camera::getProjection()
{
    return projection;
}

glm::mat4 Camera::getView()
{
    return view;
}

GameObject* Camera::getTarget()
{
    return target;
}

CameraMode Camera::getcamMode()
{
    return mode;
}
