#include "src/headers/Functions.h"
#include "src/headers/ObjectHandler.h"
#include "src/headers/Init.h"
#include "src/headers/Globals.h"
#include "src/headers/Camera.h"
#include "src/headers/Terrain.h"
#include "src/headers/Sol.h"
#include "src/headers/Glider.h"

#include "src/headers/Textbox2.h"
#include "src/headers/logger.h"
#include <iostream>
#include <string>

#define AUDIO_IMPLEMENTATION
#include "src/headers/Audio.hpp"

//Object
Glider* glider;
Sol* sun;
Terrain* map;
Light* light;
GameObject* destination;

//Mesh
Mesh* sunMesh;
Mesh* pacman;
Mesh* object;
Mesh* gliderMesh;
Mesh* heightMap;

//Texture
Texture* texTest;
Texture* defaultTexture;
Texture* gliderTexture;

//Shader
Shader* heightMapShader;
Shader* textShader;
Shader* BaWShader;
Shader* standardShader;


//int hour;
float minute;
std::string currentTime;
float lastDiff;


TextBox seasonText;
TextBox speedText;
TextBox dayTimeText;
TextBox positionXText;
TextBox positionYText;
TextBox positionZText;
TextBox destinationText;

TTF_Font* font;
SDL_Color white = { 255, 255, 255, 255 };
SDL_Color red = { 255, 0, 0, 255 };
SDL_Color green = { 0, 255, 0, 255 };

//TODO delete movable class

//--------------------------------------------------------------------
//	USED TO CHANGE POSITION OF SUN(LIGHTSOURCE) FOR DAYTIME CYCLE
//--------------------------------------------------------------------
void updateTime()
{
	//Update clock:
	//use g_deltatime......wow fps stuff..
	if (g_changingDayTime)
	{
		if (minute >= 60)
		{
			minute = 0;
			if (g_hour >= 24)
			{
				g_hour = 0;
			}
			else
			{
				g_hour += 1;
			}
		}
		else
		{
			minute += 0.49f;
		}
	}

	dayTimeText.draw("Hour: " + std::to_string(g_hour), white, glm::vec3(3, 5, 2), center);


			//----------------------------------------------------------------
			// Rotate Sun around "planet" (yay flatearthers xD)
			//----------------------------------------------------------------
			if (g_changingDayTime)
			{
				int speed = 20;
				float s = sin(0.0001f * speed);
				float c = cos(0.0001f * speed);
				sun->transform.position.x -= map->transform.position.x;
				sun->transform.position.y -= map->transform.position.y - 150;		// constant number gives a decent position considering the displace modifier done to the map.

				float tempx = sun->transform.position.x * c - sun->transform.position.y * s;
				float tempy = sun->transform.position.x * s + sun->transform.position.y * c;

				sun->transform.position.y = tempy + map->transform.position.y - 150;
				sun->transform.position.x = tempx + map->transform.position.x;

				//Light color:
				float value = (500 + sun->transform.position.y) / 1000;
				lights[0]->color = glm::vec3(value, value, value);
			}

			// Light position:
			lights[0]->transform.position = sun->transform.position;



}


void updateSeason()
{
	std::string temp;
	switch ((int)season)
	{
		case 1:
		temp = "summer";
		break;
		case 2:
		temp = "autumn ";
		break;
		case 3:
		temp = "winter";
		break;
		case 4:
		temp = "spring";
		break;
		default:
		temp = "Season";
		break;
	}
	seasonText.draw(temp, white, glm::vec3(-3, 5, 2), center);
}


//--------------------------------------------------------------------
//	USED TO INITIALIZE THE GAME AND YOUR GAMEOBJECTS, CLASSES, AND FUNCTIONS
//--------------------------------------------------------------------
void Start()
{
    //Audio::InitAudio();
    //Audio::AddBGM("../resources/sound/pacman_jazz.wav", "bgm", false);

	// Mesh
	heightMap = LoadObject("gjovik2");	//newMap, gjovik2, gjovik
	gliderMesh = LoadObject("plane");
	sunMesh = LoadObject("pellet");
	pacman = LoadObject("Ghost");

	// Textures
	defaultTexture = LoadTexture("white");
	gliderTexture = LoadTexture("texture");

	// Shaders
	standardShader = LoadShader("standard");
	BaWShader = LoadShader("BaW");
	textShader = LoadShader("text");
	heightMapShader = LoadShader("height");


	// Objects
	seasonText.initBox();
	speedText.initBox();
	dayTimeText.initBox();
	positionXText.initBox();
	positionYText.initBox();
	positionZText.initBox();
	destinationText.initBox();

	destination = new GameObject();
	glider = new Glider();
	sun = new Sol();
	map = new Terrain();

	font = TTF_OpenFont("../resources/fonts/Joystix.TTF", 20);     // Loads the font


	mainCamera = new Camera(glm::vec3(0, 0, 5), glm::vec3(0, 0, -1), glm::vec3(0, 1, 0), freeCam);
	minute = 0;
	lastDiff = 1000;

	// Set values:

	sun->material.color = glm::vec4(255, 255, 0, 255);

	destination->transform.position = glm::vec3(-105, -35, 35);
	destination->material.color = glm::vec4(255, 0, 0, 255);

	// Object, mesh, texture, shader:
	InitGameObject(destination, sunMesh, defaultTexture, standardShader);
	InitGameObject(sun, sunMesh, defaultTexture, standardShader);
	InitGameObject(map, heightMap, defaultTexture, heightMapShader);
    InitGameObject(mainCamera, nullptr, nullptr, nullptr);
	InitGameObject(glider, gliderMesh, gliderTexture, standardShader);;


	light = CreateLight(glm::vec3(1.0f));
	lights[0]->transform.position = glm::vec3(-50, -20, 0);

	mainCamera->setFollowDistance(glm::vec3(0, 20, 10));
	mainCamera->setLookatPosition(glm::vec3(0, 0, 0));
	mainCamera->transform.position = glm::vec3(0, 10, 40);

	mainCamera->setTarget(glider);
    //Audio::PlayBGM("bgm", -1);		//Audio has a weird distorted sound, so it is not used for now.
}

//--------------------------------------------------------------------
//	USED TO UPDATE THE GAME, OTHERWISE THE SAME AS START
//--------------------------------------------------------------------
void Update()
{
	// Daylight cycle:
	updateTime();
	// Season cycle:
	updateSeason();

	glider->update();


	// Speed of glider:
	speedText.draw("Speed: " + std::to_string(round(g_speed * 100)/100).substr(0,4), white, glm::vec3(3.5f, -5, 2), center);

	// Position:
	std::string posX = "x: " + std::to_string((int)mainCamera->transform.position.x);
	std::string posY = "y: " + std::to_string((int)mainCamera->transform.position.y + 39);
	std::string posZ = "z: " + std::to_string((int)mainCamera->transform.position.z);
	positionXText.draw(posX, white, glm::vec3(-6.0f, -4.0f, 2), left);
	positionYText.draw(posY, white, glm::vec3(-6.0f, -4.5f, 2), left);
	positionZText.draw(posZ, white, glm::vec3(-6.0f, -5.0f, 2), left);


	// Destination indicator:
	glm::vec3 difference;
	glm::vec3 lookat;
	glm::vec3 newVector;

	if (mainCamera->getcamMode() == CameraMode::gliderCam)
	{
		difference = destination->transform.position - glider->transform.position;
		lookat = glider->getLookatDirection();
		newVector = glm::cross(difference, lookat);

		float newDiff = sqrt(pow(difference.x, 2) + pow(difference.y, 2) + pow(difference.z, 2));
		if (lastDiff - newDiff < 0)
		{
			destinationText.draw("Gjovik", red, glm::vec3(newVector.y / 40, newVector.z / 40, 1), center);
		}
		else
		{
			destinationText.draw("Gjovik", green, glm::vec3(newVector.y / 40, newVector.z / 40, 1), center);
		}
		lastDiff = newDiff;
	}
	else if (mainCamera->getcamMode() == CameraMode::freeCam)
	{
		difference = mainCamera->transform.position - glider->transform.position;
		lookat = mainCamera->getLookatDirection();
		newVector = glm::cross(difference, lookat);

		if (newVector.x > 0)
		{
			destinationText.draw("Glider", red, glm::vec3(-(newVector.y / 40), newVector.z / 40, 1), center);
		}
		else
		{
			destinationText.draw("Glider", green, glm::vec3(-(newVector.y / 40), newVector.z / 40, 1), center);
		}
	}
}
