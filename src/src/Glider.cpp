#include "src/headers/Glider.h"
#include "src/headers/Globals.h"
#include "src/headers/logger.h"
#include <glm/gtx/rotate_vector.hpp>

Glider::Glider()
{
    transform.rotate(90, glm::vec3(0, -1, 0), 1);
    transform.scale = glm::vec3(0.3f, 0.3f, 0.3f);

	transform.translate(glm::vec3(0, 0, 0), 1);
	restartPosition = glm::vec3(0, 0, 0);
	restartRotation = transform.rotation;
    lookatDirection = glm::vec3(0, 0, -1);
	rightDirection = glm::cross(lookatDirection, glm::vec3(0, (lookatDirection.z >= 0)? -3 : 3, 0));
}

void Glider::update()
{
    transform.position += lookatDirection * 0.01f * g_speed;
    transform.position.y -= 9.81f * g_deltatime * 0.3;    //9.81 * g_deltatime is a little too much.
}


void Glider::input(int key, int scancode, int action, int mods)
{
    float ROTATE = 3;
    // Rotate glider with WASD.
       if (key == GLFW_KEY_W )
       {
           // Rotate lookatDirection to change movement.
           lookatDirection = glm::rotate(lookatDirection, -glm::radians(ROTATE), rightDirection);
           lookatDirection = glm::normalize(lookatDirection);
           upDirection = glm::rotate(upDirection, -glm::radians(ROTATE), rightDirection);
           upDirection = glm::normalize(upDirection);

           // Rotate glider orientation for a good look(same amount of rotation as with movement rotation)
           transform.rotate(ROTATE, glm::vec3(0, 0, 1), 1);
       }
       if (key == GLFW_KEY_S)
       {

           lookatDirection = glm::rotate(lookatDirection, glm::radians(ROTATE), rightDirection);
           lookatDirection = glm::normalize(lookatDirection);
           upDirection = glm::rotate(upDirection, glm::radians(ROTATE), rightDirection);
           upDirection = glm::normalize(upDirection);

           transform.rotate(-ROTATE, glm::vec3(0, 0, 1), 1);
       }
       if (key == GLFW_KEY_A)
       {
            upDirection = glm::rotate(upDirection, -glm::radians(ROTATE), lookatDirection);
            upDirection = glm::normalize(upDirection);
            rightDirection = glm::rotate(rightDirection, -glm::radians(ROTATE), lookatDirection);
            rightDirection = glm::normalize(rightDirection);
            transform.rotate(ROTATE, glm::vec3(1.0f, 0.0f, 0.0f), 1);
       }
       if (key == GLFW_KEY_D)
       {
           upDirection = glm::rotate(upDirection, glm::radians(ROTATE), lookatDirection);
           upDirection = glm::normalize(upDirection);
           rightDirection = glm::rotate(rightDirection, glm::radians(ROTATE), lookatDirection);
           rightDirection = glm::normalize(rightDirection);
           transform.rotate(-ROTATE, glm::vec3(1.0f, 0.0f, 0.0f), 1);
       }
       if (key == GLFW_KEY_COMMA)
       {
           if (g_speed < 20)
                g_speed += 0.1f;
       }
       if (key == GLFW_KEY_PERIOD)
       {
           if (g_speed > 3)
                g_speed -= 0.1f;
       }

       if (key == GLFW_KEY_F)   //Move glider around in the scene:
       {
           transform.position = glm::vec3((rand() % 150) - 50, (rand() % 100) - 20, 0);
       }

       if (key == GLFW_KEY_R && action == GLFW_PRESS)  // reset glider position
       {
           transform.position = restartPosition;
           /*transform.rotation = restartRotation;
           lookatDirection = glm::vec3(0, 0, -1);*/
       }
}

void Glider::setLookatDirection(glm::vec3 newDirection)
{
    lookatDirection = newDirection;
}

glm::vec3 Glider::getLookatDirection()
{
    return lookatDirection;
}
