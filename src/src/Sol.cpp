#include "src/headers/Sol.h"
#include "src/headers/Globals.h"
#include "src/headers/logger.h"

#include "src/headers/ObjectHandler.h"//1 of these 3 is needed
#include "src/headers/Init.h"
#include "src/headers/Camera.h"

Sol::Sol()
{
    transform.position = glm::vec3(0, 500, -50);
    transform.scale = glm::vec3(10, 10, 10);
}

void Sol::update()
{

}

void Sol::rotateSun(float angle)
{
    float s = sin(angle);
    float c = cos(angle);
    transform.position = glm::vec3(0, 600, 0); //Base position.
    // Rotates sun around center of map (0, 0, 0):

    float tempx = transform.position.x * c - transform.position.y * s;
    float tempy = transform.position.x * s + transform.position.y * c;

    transform.position.y = tempy;
    transform.position.x = tempx;

    //Change light intensity depending on where the sun:
    float value = (1000 + transform.position.y) / 1000;
    lights[0]->color = glm::vec3(value, value, value);
}

void Sol::input(int key, int scancode, int action, int mods)
{

    if (key == GLFW_KEY_6 && action == GLFW_RELEASE)
    {
        //Specific for each daytime:
        g_hour = 12;
        float angle = 0.0f;

        // Identical code for each daytime:
        rotateSun(angle);
    }

    if (key == GLFW_KEY_7 && action == GLFW_RELEASE)
    {
        //Specific for each daytime:
        g_hour = 18;
        float angle = 1.2f;

        // Identical code for each daytime:
        rotateSun(angle);
    }

    if (key == GLFW_KEY_8 && action == GLFW_RELEASE)
    {
        //Specific for each daytime:
        g_hour = 0;
        float angle = 3.1f;

        // Identical code for each daytime:
        rotateSun(angle);
    }

    if (key == GLFW_KEY_9 && action == GLFW_RELEASE)
    {
        //Specific for each daytime:
        g_hour = 6;
        float angle = 5.0f;

        // Identical code for each daytime:
        rotateSun(angle);
    }

    if (key == GLFW_KEY_0 && action == GLFW_RELEASE)
    {
        g_changingDayTime = !g_changingDayTime;
    }
}
