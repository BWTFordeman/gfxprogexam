#include "src/headers/Globals.h"

int SCREEN_WIDTH = 1200;
int SCREEN_HEIGHT = 1000;

double g_deltatime;
Camera* mainCamera;
int g_hour = 12;
bool g_changingDayTime;
float dayTime;
float season;
bool g_hardline;
float g_textSize = 0.2f;
float g_speed = 3;
float g_FOV = 45;
float g_viewDistance = 1000.0f;
int g_fog = 0;
