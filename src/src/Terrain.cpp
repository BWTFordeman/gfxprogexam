#include "src/headers/Terrain.h"
#include "src/headers/Globals.h"
#include "src/headers/logger.h"

Terrain::Terrain()
{
    transform.position = glm::vec3(0, 0, 0);
	transform.scale = glm::vec3(1000, 900, 1000);
}

void Terrain::update()
{
    if (changingSeason)
    {
        season += (float)g_deltatime;
    }

    if (season > 5)
     {
         season = 1;
     }
}

void Terrain::input(int key, int scancode, int action, int mods)
{
    if (key == GLFW_KEY_O && action == GLFW_RELEASE)
    {
        g_hardline = !g_hardline;
    }
    if (key == GLFW_KEY_1 && action == GLFW_RELEASE)
    {
        season = 1;
    }
    if (key == GLFW_KEY_2 && action == GLFW_RELEASE)
    {
        season = 2;
    }
    if (key == GLFW_KEY_3 && action == GLFW_RELEASE)
    {
        season = 3;
    }
    if (key == GLFW_KEY_4 && action == GLFW_RELEASE)
    {
        season = 4;
    }
    if (key == GLFW_KEY_5 && action == GLFW_RELEASE)
    {
        changingSeason = !changingSeason;
    }
    if (key == GLFW_KEY_E && action == GLFW_RELEASE)
    {
        g_fog = (g_fog == 1)? 0 : 1;
    }
}
