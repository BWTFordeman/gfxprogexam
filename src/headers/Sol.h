#pragma once
#include "GameObject.h"

class Sol : public GameObject
{
private:

public:
    bool changingDayTime = true;
    Sol();

    virtual void update();
    virtual void input(int key, int scancode, int action, int mods);
    void rotateSun(float angle);
};
