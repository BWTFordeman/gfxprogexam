#pragma once
#include "src/headers/Camera.h"

extern double g_deltatime;
extern Camera* mainCamera;
extern int g_hour;
extern bool g_changingDayTime;
extern float dayTime;
extern float season;
extern bool g_hardline;
extern float g_textSize;
extern float g_speed;
extern float g_FOV;
extern float g_viewDistance;
extern int g_fog;
