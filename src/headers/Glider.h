#include "GameObject.h"

class Glider : public GameObject
{
private:
    glm::vec3 lookatDirection;

public:
    glm::vec3 upDirection;
    glm::vec3 rightDirection;
    glm::vec3 restartPosition;
    glm::mat4 restartRotation;

    Glider();
    virtual void update();
    virtual void input(int key, int scancode, int action, int mods);


    void setLookatDirection(glm::vec3 newDirection);

    glm::vec3 getLookatDirection();
};
