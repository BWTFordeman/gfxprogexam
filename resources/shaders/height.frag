#version 430 core

in vec2 TexCoord;
in vec3 fragNormal;
in vec3 fragVert;
in vec4 viewSpace;

out vec4 FragColor;

uniform vec3 camPos;
uniform vec3 lightPos;
uniform mat4 model;
uniform sampler2D ourTexture;
uniform vec3 lightColor;
uniform vec4 vertexColor;
uniform float specularity;

uniform int hardLines;
uniform float minH;
uniform float maxH;
uniform vec4 iceColor;
uniform vec4 waterColor;		// = vec4(.25f,  .5f,  1f,     1f);
uniform vec4 sandColor;			// = vec4(.761f, .698f, .502f, 1f);
uniform vec4 vegetationColor;		// = vec4(.2f,  1.0f,   .2f,   1f);
uniform vec4 mountainColor;		// = vec4(.45f,  .45f,  .1f,   1f);
uniform vec4 snowColor;			// = vec4(.8f,   .9f,  1f,     1f);

uniform int fog;
const vec3 fogColor = vec3(0.5, 0.5,0.5);


void main() {
	// Invisible if alhpa is low
	vec4 tex = texture(ourTexture, TexCoord) * vertexColor;
	if(tex.a < 0.1)
		discard;

	// Ambient
	float ambientStrength = 0.5;
	vec3 ambient = ambientStrength * lightColor;


	// Diffuse
	mat3 normalMatrix = transpose(inverse(mat3(model)));
	vec3 normal = normalize(normalMatrix * fragNormal);
	
	vec3 fragPosition = vec3(model * vec4(fragVert, 1.0));
	vec3 surfaceToLight = normalize(lightPos - fragPosition);

	float brightness = max(dot(normal, surfaceToLight), 0.1);
	vec3 diffuse = brightness * lightColor;


	// specular
	vec3 viewDir = normalize(camPos - fragVert);
	vec3 reflectDir = reflect(-surfaceToLight, normal);
	float spec = 0.0;
	vec3 halfwayDir = normalize(surfaceToLight + viewDir);  
	spec = pow(max(dot(normal, halfwayDir), 0.0), specularity);
	vec3 specular = spec * lightColor;    
	
	float dist = distance(lightPos, fragVert);
	float attenuation = 1.0f / (1.0 + (0.03125 * dist) + (0.0 * dist * dist));

	// final colors
	vec4 surfaceColor; //= texture(ourTexture, TexCoord) * vertexColor;

	float midH1 = (minH+maxH)/2.0f;
	float midH2 = ((minH+maxH)/3.5f);
	float pos = fragPosition.y;

	if (pos < minH)
	{
		float t = minH/maxH;
		surfaceColor = waterColor * 3 - 1 + (iceColor / (t * 14 - 5.0f));
	}
	else if (pos < midH1)
	{	
		float t = 0;
		if (hardLines == 0)
			t = (pos-minH)/(midH1-minH);
		surfaceColor = (1.0f-t) * sandColor + t * vegetationColor;
	}
	else if (pos < midH2)
	{
		float t = 0;
		if (hardLines == 0)
			t = (pos-midH1)/(midH2-midH1);
		surfaceColor = (1.0f-t) * vegetationColor + t * mountainColor;
	}
	else if (pos < maxH)
	{
		float t = 0;
		if (hardLines == 0)
			t = (pos-midH2)/(maxH-midH2);
		surfaceColor = (1.0f-t) * mountainColor + t * snowColor;
	}
	else 
	{
		surfaceColor = snowColor;
	}


	//fog:
	if (fog == 1)
	{	
		//get all lights and texture
		vec3 lightColor = (ambient + diffuse * attenuation) * surfaceColor.rgb + specular * attenuation;
	 
		vec3 finalColor = vec3(0, 0, 0);
	
		//distance
		float fogFactor = 0;
	
		dist = abs(viewSpace.z);
	
		// 100 - fog starts; 300 - fog ends
		fogFactor = (300 - dist)/(300 - 150);
		fogFactor = clamp( fogFactor, 0.0, 1.0 );
		 
		finalColor = mix(fogColor, lightColor, fogFactor);

		FragColor = vec4(finalColor, 1);
	}
	else
	{
		FragColor = vec4((ambient + diffuse * attenuation) * surfaceColor.rgb + specular * attenuation, 1);
	}



}
