#include "TileMap.h"

#include "logger.h"
#include <vector>
#include "mad.h"
#include "Pacman.h"

int tiles[36][28];

extern glm::mat4 g_view;
extern glm::vec3 camPos;

extern Pacman pacman;

// Unused constructor
TileMap::TileMap() {}

// Default constructor we use
TileMap::TileMap(Material* mat, Mesh* m, Buffer* buff, const char* map_path) : Entity()
{
    material = mat;
    buffer = buff;
    mesh = m;

    // Read map data
    std::string map_data;
    mad::readStringFromFile(map_path, map_data);

    // Mesh creation
    int x = 0;
    int y = 35;

    std::vector<glm::vec3> vert;
    std::vector<glm::vec2> tex;
    std::vector<glm::uvec3> ind;
    std::vector<glm::vec3> norm;

    // Adds a cube to the vector for every wall
    for (char& c : map_data)
    {
        switch (c)
        {
        case '1':
            // Vertices 
            // Front
            vert.push_back(glm::vec3( 0.5f + x,  0.5f + y, 0.5f));
            vert.push_back(glm::vec3( 0.5f + x, -0.5f + y, 0.5f));
            vert.push_back(glm::vec3(-0.5f + x, -0.5f + y, 0.5f));
            vert.push_back(glm::vec3(-0.5f + x,  0.5f + y, 0.5f));

            // Right
            vert.push_back(glm::vec3(0.5f + x,  0.5f + y, -0.5f));
            vert.push_back(glm::vec3(0.5f + x, -0.5f + y, -0.5f));
            vert.push_back(glm::vec3(0.5f + x, -0.5f + y,  0.5f));
            vert.push_back(glm::vec3(0.5f + x,  0.5f + y,  0.5f));

            // Back
            vert.push_back(glm::vec3( 0.5f + x,  0.5f + y, -0.5f));
            vert.push_back(glm::vec3( 0.5f + x, -0.5f + y, -0.5f));
            vert.push_back(glm::vec3(-0.5f + x, -0.5f + y, -0.5f));
            vert.push_back(glm::vec3(-0.5f + x,  0.5f + y, -0.5f));

            // Left
            vert.push_back(glm::vec3(-0.5f + x,  0.5f + y,  0.5f));
            vert.push_back(glm::vec3(-0.5f + x, -0.5f + y,  0.5f));
            vert.push_back(glm::vec3(-0.5f + x, -0.5f + y, -0.5f));
            vert.push_back(glm::vec3(-0.5f + x,  0.5f + y, -0.5f));

            // Top
            vert.push_back(glm::vec3( 0.5f + x, 0.5f + y, -0.5f));
            vert.push_back(glm::vec3( 0.5f + x, 0.5f + y,  0.5f));
            vert.push_back(glm::vec3(-0.5f + x, 0.5f + y,  0.5f));
            vert.push_back(glm::vec3(-0.5f + x, 0.5f + y, -0.5f));

            // Bottom
            vert.push_back(glm::vec3( 0.5f + x, -0.5f + y,  0.5f));
            vert.push_back(glm::vec3( 0.5f + x, -0.5f + y, -0.5f));
            vert.push_back(glm::vec3(-0.5f + x, -0.5f + y, -0.5f));
            vert.push_back(glm::vec3(-0.5f + x, -0.5f + y,  0.5f));

            // Normals //
            // Front
            norm.push_back(glm::vec3( 0.5773491859436035,  0.5773491859436035,  0.5773491859436035));
            norm.push_back(glm::vec3( 0.5773491859436035, -0.5773491859436035,  0.5773491859436035));
            norm.push_back(glm::vec3(-0.5773491859436035, -0.5773491859436035,  0.5773491859436035));
            norm.push_back(glm::vec3(-0.5773491859436035,  0.5773491859436035,  0.5773491859436035));

            // Right
            norm.push_back(glm::vec3(0.5773491859436035,  0.5773491859436035, -0.5773491859436035));
            norm.push_back(glm::vec3(0.5773491859436035, -0.5773491859436035, -0.5773491859436035));
            norm.push_back(glm::vec3(0.5773491859436035, -0.5773491859436035,  0.5773491859436035));
            norm.push_back(glm::vec3(0.5773491859436035,  0.5773491859436035,  0.5773491859436035));

            // Back
            norm.push_back(glm::vec3( 0.5773491859436035,  0.5773491859436035, -0.5773491859436035));
            norm.push_back(glm::vec3( 0.5773491859436035, -0.5773491859436035, -0.5773491859436035));
            norm.push_back(glm::vec3(-0.5773491859436035, -0.5773491859436035, -0.5773491859436035));
            norm.push_back(glm::vec3(-0.5773491859436035,  0.5773491859436035, -0.5773491859436035));

            // Left
            norm.push_back(glm::vec3(-0.5773491859436035,  0.5773491859436035,  0.5773491859436035));
            norm.push_back(glm::vec3(-0.5773491859436035, -0.5773491859436035,  0.5773491859436035));
            norm.push_back(glm::vec3(-0.5773491859436035, -0.5773491859436035, -0.5773491859436035));
            norm.push_back(glm::vec3(-0.5773491859436035,  0.5773491859436035, -0.5773491859436035));

            // Top
            norm.push_back(glm::vec3( 0.5773491859436035, 0.5773491859436035, -0.5773491859436035));
            norm.push_back(glm::vec3( 0.5773491859436035, 0.5773491859436035,  0.5773491859436035));
            norm.push_back(glm::vec3(-0.5773491859436035, 0.5773491859436035,  0.5773491859436035));
            norm.push_back(glm::vec3(-0.5773491859436035, 0.5773491859436035, -0.5773491859436035));

            // Bottom
            norm.push_back(glm::vec3( 0.5773491859436035, -0.5773491859436035,  0.5773491859436035));
            norm.push_back(glm::vec3( 0.5773491859436035, -0.5773491859436035, -0.5773491859436035));
            norm.push_back(glm::vec3(-0.5773491859436035, -0.5773491859436035, -0.5773491859436035));
            norm.push_back(glm::vec3(-0.5773491859436035, -0.5773491859436035,  0.5773491859436035));

            tiles[y][x] = 1;            // Updates tiles array
            x++;
            break;
        case '0':
            tiles[y][x] = 0;            // Updates tiles array
            x++;
            break;
        case '\n':
            y--;
            x = 0;
            break;
        case '2':
            tiles[y][x] = 0;            // Updates tiles array
            x++;
            break;
        case '3':
            tiles[y][x] = 3;            // Updates tiles array
            x++;
            break;
        case '4':
            tiles[y][x] = 4;            // Updates tiles array
            x++;
            break;
        case '5':
            tiles[y][x] = 5;            // Updates tiles array
            x++;
            break;
        }

    }

    // Storing texture coordinates
    for (size_t i = 0; i < vert.size() / 4; i++)
    {
        tex.push_back(glm::vec2(1.0f, 1.0f));
        tex.push_back(glm::vec2(1.0f, 0.0f));
        tex.push_back(glm::vec2(0.0f, 0.0f));
        tex.push_back(glm::vec2(0.0f, 1.0f));
    }

    // Storing vertices
    for (size_t i = 0; i < vert.size(); i+=4)
    {
        ind.push_back(glm::uvec3(i + 3, i + 1, i));
        ind.push_back(glm::uvec3(i + 3, i + 2,  i + 1));
    }


    glGenVertexArrays(1, &buffer->VAO);
    glBindVertexArray(buffer->VAO);

    glGenBuffers(3, &buffer->VBO[0]);                                           // Generates unique ID for buffer object
    
    glBindBuffer(GL_ARRAY_BUFFER, buffer->VBO[0]);                              // Binds unique buffer to a target, GL_ARRAY_BUFFER here
    glBufferData(GL_ARRAY_BUFFER, (vert.size() * sizeof(glm::vec3)), vert.data(), GL_STATIC_DRAW);
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, (void*)0);               // How to interpret the vertex data
    // Which vertex attribute, number of dimensions in the vector, type of data, normalized or not, space between attribute sets, offset

    glEnableVertexAttribArray(0); // <-------------------------------- Remember 

    glBindBuffer(GL_ARRAY_BUFFER, buffer->VBO[1]);                              // Binds unique buffer to a target, GL_ARRAY_BUFFER here
    glBufferData(GL_ARRAY_BUFFER, (tex.size() * sizeof(glm::vec2)), tex.data(), GL_STATIC_DRAW);
    glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 0, (void*) 0);              // How to interpret the vertex data
    // Which vertex attribute, number of dimensions in the vector, type of data, normalized or not, space between attribute sets, offset

    glEnableVertexAttribArray(1);

    glBindBuffer(GL_ARRAY_BUFFER, buffer->VBO[2]);                              // Binds unique buffer to a target, GL_ARRAY_BUFFER here
    glBufferData(GL_ARRAY_BUFFER, (norm.size() * sizeof(glm::vec3)), norm.data(), GL_STATIC_DRAW);
    glVertexAttribPointer(2, 3, GL_FLOAT, GL_FALSE, 0, (void*)0);               // How to interpret the vertex data
    // Which vertex attribute, number of dimensions in the vector, type of data, normalized or not, space between attribute sets, offset

    glEnableVertexAttribArray(2);

    glGenBuffers(1, &buffer->IBO);                                              // Generates unique ID for Element object
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, buffer->IBO);                         // Binds unique buffer to a target, GL_ELEMENT_ARRAY_BUFFER here
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, ind.size() * sizeof(glm::uvec3), ind.data(), GL_STATIC_DRAW);
    // Copies ind data into the GL_ELEMENT_ARRAY_BUFFER target

    // Unbinds everything
    glBindVertexArray(0);
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);

    // Update mesh
    mesh->vertices = vert;
    mesh->texture_coordinates = tex;
    mesh->indices = ind;
    mesh->normals = norm;
}

void TileMap::draw()
{
    material->shader->use();                                    // Uses bound shader
    material->shader->setMatrix("model", transform.rotation);   // Sets model uniform in shdaer
    material->shader->setMatrix("view", g_view);                // Sets view uniform in shader

    // Sets light position uniform in shader
    material->shader->setFloat("lightPos", pacman.transform.position.x, pacman.transform.position.y, 5.0f);
    // Sets camera position uniform in shader
    //material->shader->setFloat("camPos", pacman.transform.position.x, pacman.transform.position.y, 25.0f);
    material->shader->setFloat("camPos", camPos.x, camPos.y, camPos.z);

    glBindTexture(GL_TEXTURE_2D, material->texture->getNo());   // Binds texture for draw
    glBindVertexArray(buffer->VAO);                             // Bind VAO for draw
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, buffer->IBO);         // Binds IBO for draw
    glDrawElements(GL_TRIANGLES, mesh->indices.size() * sizeof(glm::uvec3), GL_UNSIGNED_INT, 0); // Draws elements
}

void TileMap::update(double delta_time)
{
    // Rotation
    transform.rotate(0.0f, glm::vec3(0.5f, 1.0f, 0.0f), delta_time);
}