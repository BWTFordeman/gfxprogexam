#pragma once
#include <string>

#ifdef _WIN32
#include "Dependencies\glew-2.1.0\include\GL\glew.h"
#include "Dependencies\SDL2_ttf-2.0.14\include\SDL_ttf.h"
#else
#include <GL/glew.h>
#include <SDL2/SDL_ttf.h>
#endif

class Texture
{
private:
    GLuint m_texture_number;

    SDL_Surface *message;
public:
    Texture();
    Texture(const char* path);

    int createTextureFromText(TTF_Font* font, std::string text, SDL_Color color);

    GLuint getNo();
};
