#version 430 core

layout (location = 0) in vec3 position;
layout (location = 1) in vec2 texCoord;
layout (location = 2) in vec3 normals;

out vec3 vertexColor;
out vec2 TexCoord;
out vec3 fragNormal;
out vec3 fragVert;

uniform mat4 projection;
uniform mat4 view;
uniform mat4 model;
uniform int ghostID;

void main() {
	vec3 objectColor;

	switch (ghostID) 
	{
	case 1: 
		objectColor = vec3(1.0, 0.0,  0.0); 
		break;
	case 2: 
		objectColor = vec3(1.0, 0.6,  0.0); 
		break;
	case 3: 
		objectColor = vec3(0.0, 1.0,  1.0); 
		break;
	case 4: 
		objectColor = vec3(1.0, 0.75, 0.8); 
		break;
	case 5:
		objectColor = vec3(0.0, 0.0, 0.55);
		break;
	}
   
	fragVert = position;
	fragNormal = normals;
	vertexColor = objectColor;
	TexCoord = texCoord;
	gl_Position = projection * view * model * vec4(position, 1.0);
}