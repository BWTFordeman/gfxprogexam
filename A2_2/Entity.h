#pragma once
#include "Structs.h"

// Base class for Entities in the project

class Entity
{
public:
    Transform transform;        // Position and rotation Struct

    Entity();                   

    virtual void draw();
    virtual void update();
};
