# imt3673-project
Done by:
	Christian Bråthen Tverberg - 473185.
	Maarten Dijkstra           - 473201.
	Nataniel Gåsøy             - 131390.


### The project
The final hand-in and project for the IMT2531 subject. This project uses mainly glfw in order to make 3D packman in openGL. SDL is used for audio and ttf text handling. 

The program:
	- Reads the provided map layout from .txt file and renders it on screen, with textured walls.
	- Presents a menu that allows player to start or exit the game.
	- Pauses the game and presents a pause menu if esc is pressed during gameplay.
	- Maintain player score and number of lives left.
	- Collectible items are removed from the map when collected (rendered as cubes). 
	- Has moving ghosts (ghosts changes direction randomly if a crossroad is encountered).
	- Tilebased collision for walls, circular collision for ghosts. 
	- Pacman is loaded from a model, and is textured (rolls like a ball when moving).
	- Can change camera perspective.
	- Uses a basic lighting model (Phong model).
	- Has different audio for the different screens, as well as sound effects.


## Controls
Pacman is controlled using WASD keys.
Menu options is controlled with the arrow keys.
Press the "C" key to browse through camera modes.


### Note
The program should run on both linux and windows. 

Windows: Visual studio was used as the development platform.
Linux:   Enter the "imt2531-oblig2" folder, and run "make && ./a.out".
