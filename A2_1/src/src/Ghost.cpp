#include "Ghost.h"
#include "Functions.h"
#include "Globals.h"
#include "Audio.h"

Ghost::Ghost(int x, int y, float s, int i) : Creature(x, y, s)
{
    id = i;	//	Set id
}

void Ghost::draw()
{
	anim = (int)(animTimer*4) % 2;	//	Set animation frame

	//	Set texture
	if (dead)
		model[0].sprite[0]->setTexture(tEyes);
	else if (scared)
	{
		if (anim == 0)
			model[0].sprite[0]->setTexture(tScaredBlue);
		else if (scareTimer < 1.5f)
			model[0].sprite[0]->setTexture(tScaredWhite);
	}
	else
		model[0].sprite[0]->setTexture(tGhosts[id]);

	//	Set direction
	glm::vec3 facing;
	switch (dir)
	{
	case Up:	facing = glm::vec3(0, 0, PI);	break;
	case Left:	facing = glm::vec3(0, 0, PI / 2);	break;
	case Right: facing = glm::vec3(0, 0, PI / 2 * 3);	break;
	case Down:	facing = glm::vec3(0, 0, 0);	break;
	default:	facing = glm::vec3(0, 0, 0);	break;
	}

	//	Update model
	model[0].setVertices(
		0,
		glm::vec3(-((float)(levelData.size()) / 2) + (posX + moveX), ((float)(levelData[0].size()) / 2) - (posY + moveY), 0),
		glm::vec3(.5f, .5f, .5f),
		facing);
	model[0].sprite[0]->setCamera(cameraFocus, cameraPosition);
	model[0].sprite[0]->draw();
}

void Ghost::move()
{
	//	Whether the ghost should check for value 6 tiles when moving
	bool check6 = true;
	if (levelData[posX][posY] == 5 || levelData[posX][posY] == 6 || dead)
		check6 = false;

	if (dir == None || !checkDirection(dir, posX, posY, check6))
		newDirection();

	//	Set speed
	if (dead)
		speed = ghostSpeed * 1.5f;
	else if (levelData[posX][posY] == 4)
		speed = ghostSpeed / 2.0f;
	else if (scared)
		speed = ghostSpeed / 1.5f;
	else
		speed = ghostSpeed;

	//	Reached ghost house, no longer dead
	if (levelData[posX][posY] == 5)
		dead = false;

	//	Move towards next position
	if (dir == Right && checkDirection(dir, posX, posY, check6))
	{
		moveX += speed * deltaTime * 60;
		moveY = 0;
	}
	if (dir == Left && checkDirection(dir, posX, posY, check6))
	{
		moveX -= speed * deltaTime * 60;
		moveY = 0;
	}
	if (dir == Down && checkDirection(dir, posX, posY, check6))
	{
		moveX = 0;
		moveY += speed * deltaTime * 60;
	}
	if (dir == Up && checkDirection(dir, posX, posY, check6))
	{
		moveX = 0;
		moveY -= speed * deltaTime * 60;
	}

	//	Moved to next position, find new direction
	if (dir == Right && moveX > 1)
	{
		if (posX + 1 == levelData.size())
			posX = 0;
		else if (checkDirection(dir, posX, posY, check6))
			posX++;

		if (checkDirection(dir, posX, posY, check6))
			moveX--;
		else
			moveX = 0;
		newDirection();
	}
	if (dir == Left && moveX < -1)
	{
		if (posX - 1 == -1)
			posX = levelData.size() - 1;
		else if (checkDirection(dir, posX, posY, check6))
			posX--;

		if (checkDirection(dir, posX, posY, check6))
			moveX++;
		else
			moveX = 0;
		newDirection();
	}
	if (dir == Down && moveY > 1)
	{
		if (posY + 1 == levelData[posX].size())
			posY = 0;
		else if (checkDirection(dir, posX, posY, check6))
			posY++;

		if (checkDirection(dir, posX, posY, check6))
			moveY--;
		else
			moveY = 0;
		newDirection();
	}
	if (dir == Up && moveY < -1)
	{
		if (posY - 1 == -1)
			posY = levelData[posX].size() - 1;
		else if (checkDirection(dir, posX, posY, check6))
			posY--;

		if (checkDirection(dir, posX, posY, check6))
			moveY++;
		else
			moveY = 0;
		newDirection();
	}

	//	Check for collision with Pacman
	checkPacman();
}

void Ghost::newDirection()
{
	if (dir == None)
		dir = Left;
	else
	{
		//	Whether the ghost should check for value 6 tiles when moving
		bool check6 = true;
		if (levelData[posX][posY] == 5 || levelData[posX][posY] == 6 || dead)
			check6 = false;

		//	Set movement directions, remove backwards direction
		bool directions[4]{ 1,1,1,1 }; //Left, Right, Up, Down
		if (dir == Right)
			directions[0] = false; //Can't go left
		if (dir == Left)
			directions[1] = false; //Can't go right
		if (dir == Down)
			directions[2] = false; //Can't go up
		if (dir == Up)
			directions[3] = false; //Can't go down

		//	Remove blocked directions
		if (!checkDirection(Left, posX, posY, check6)) //Left blocked
			directions[0] = false;
		if (!checkDirection(Right, posX, posY, check6)) //Right blocked
			directions[1] = false;
		if (!checkDirection(Up, posX, posY, check6)) //Up blocked
			directions[2] = false;
		if (!checkDirection(Down, posX, posY, check6)) //Down blocked
			directions[3] = false;

		//std::cout << "Available directions: " << directions[0] << "," << directions[1] << "," << directions[2] << "," << directions[3] << "," << std::endl;

		//	Find shortest path
		int shortest = -1;
		float distance = 100;
		float d = 100;

		if (directions[0])
			if ((d = findDistance(posX - 1, posY)) < distance)
			{
				distance = d;
				shortest = 0;
			}
		if (directions[1])
			if ((d = findDistance(posX + 1, posY)) < distance)
			{
				distance = d;
				shortest = 1;
			}
		if (directions[2])
			if ((d = findDistance(posX, posY - 1)) < distance)
			{
				distance = d;
				shortest = 2;
			}
		if (directions[3])
			if ((d = findDistance(posX, posY + 1)) < distance)
			{
				distance = d;
				shortest = 3;
			}

		//	Set direction
		if (shortest == 0)
			dir = Left;
		if (shortest == 1)
			dir = Right;
		if (shortest == 2)
			dir = Up;
		if (shortest == 3)
			dir = Down;
	}
}

void Ghost::changePhase()
{
	if (state == Chase)
		state = Scatter;
	else
		state = Chase;

	reverse();
}

void Ghost::reverse()
{
	if (dir == Right)
		dir = Left;
	else if (dir == Left)
		dir = Right;
	else if (dir == Up)
		dir = Down;
	else if (dir == Down)
		dir = Up;
}

void Ghost::checkPacman()
{
	//	If Pacman is in range
	if (sqrtf
	(((pacmanHandler.pacman.posX + pacmanHandler.pacman.moveX) - (posX + moveX)) * ((pacmanHandler.pacman.posX + pacmanHandler.pacman.moveX) - (posX + moveX))
		+ ((pacmanHandler.pacman.posY + pacmanHandler.pacman.moveY) - (posY + moveY)) * ((pacmanHandler.pacman.posY + pacmanHandler.pacman.moveY) - (posY + moveY))
	) < .75f)
	{
		if (scared)
		{
			PlayEffect("pacman_eatghost");
			scared = false;
			dead = true;
			ghostsEaten++;
			int bonus = 200;
			if (ghostsEaten == 2)
				bonus = 400;
			if (ghostsEaten == 3)
				bonus = 800;
			if (ghostsEaten == 4)
				bonus = 1600;
			score += bonus;
		}
		else if (!dead)
		{
			pacmanHandler.die();
			pacmanHandler.animDeathTimer = 0;
		}
	}
}

float Ghost::findDistance(float x1, float y1)
{
	float x2 = 0;
	float y2 = 0;

	if (active && !dead && (levelData[posX][posY] == 5 || levelData[posX][posY] == 6))
	{	//	Loacted inside ghost house, trying to move out
		x2 = 13;
		y2 = 14;
	}
	else if (!active || dead)
	{	//	Moving towards centre of ghost house
		x2 = 14;
		y2 = 17;
	}
	else if (scared)
	{	//	Random movement when scared
		x2 = rand() % levelData.size();
		y2 = rand() % levelData[0].size();
	}
	else if (state == Chase)
	{
		if (id == 0)	//	Red, moves towards Pacman
		{
			x2 = pacmanHandler.pacman.posX;
			y2 = pacmanHandler.pacman.posY;
		}
		if (id == 1)	//	Pink, moves towards 4 tiles infront of Pacman
		{
			int x = 0;
			int y = 0;
			if (pacmanHandler.pacman.dir == Right)
				x = 4;
			if (pacmanHandler.pacman.dir == Left)
				x = -4;
			if (pacmanHandler.pacman.dir == Down)
				y = 4;
			if (pacmanHandler.pacman.dir == Up)
				y = -4;
			x2 = pacmanHandler.pacman.posX + x;
			y2 = pacmanHandler.pacman.posY + y;
		}
		if (id == 2)	//	Blue, it's complicated
		{
			int x = 0;
			int y = 0;
			if (pacmanHandler.pacman.dir == Right)
				x = 2;
			if (pacmanHandler.pacman.dir == Left)
				x = -2;
			if (pacmanHandler.pacman.dir == Down)
				y = 2;
			if (pacmanHandler.pacman.dir == Up)
				y = -2;
			x2 = ghostHandler.ghosts[0].posX + (pacmanHandler.pacman.posX + x - ghostHandler.ghosts[0].posX) * 2;
			y2 = ghostHandler.ghosts[0].posY + (pacmanHandler.pacman.posY + y - ghostHandler.ghosts[0].posY) * 2;
		}
		if (id == 3)	//	Orange, move towards Pacman, if within 8 tiles moves towards lower left
		{
			if (sqrtf((pacmanHandler.pacman.posX - posX) * (pacmanHandler.pacman.posX - posX) + (pacmanHandler.pacman.posY - posY) * (pacmanHandler.pacman.posY - posY)) > 8)
			{
				x2 = pacmanHandler.pacman.posX;
				y2 = pacmanHandler.pacman.posY;
			}
			else
			{
				x2 = 0;
				y2 = levelData[0].size() - 1;
			}
		}
	}
	else
	{
		if (id == 0)	//	Red, upper right
		{
			x2 = levelData.size() - 1;
			y2 = 0;
		}
		if (id == 1)	//	Pink, upper left
		{
			x2 = 0;
			y2 = 0;
		}
		if (id == 2)	//	Blue, lower right
		{
			x2 = levelData.size() - 1;
			y2 = levelData[0].size() - 1;
		}
		if (id == 3)	//	Orange, lower left
		{
			x2 = 0;
			y2 = levelData[0].size() - 1;
		}
	}
	//std::cout << "(" << x1 << "," << y1 << ")" << "(" << x2 << "," << y2 << ")" << std::endl;

	//	Return distance
	return sqrtf((x2 - x1) * (x2 - x1) + (y2 - y1) * (y2 - y1));
}