#ifndef GHOST
#define GHOST

#include "Creature.h"
#include <vector>

class Ghost : public Creature
{
public:
    bool active = false;			//	Whether the ghost is active
    bool dead = false;				//	Whether the ghost is dead
    bool scared = false;			//	Whether the ghost is scared
    int id;							//	The id of the ghost

    GhostState state = Scatter;		//	The ghost's state, scatter or chase

    Ghost() : Creature(){}				//	Constructor
    Ghost(int x, int y, float s, int i);//	Constructor

	void draw();					//	Draws the ghost
	void move();					//	Moves the ghost
	void newDirection();			//	Decides on a new movement direction
	float findDistance(float x1, float y1);	//	Finds the shortest distance depending on id, state, etc...
	void changePhase();				//	Changes between scatter and chase
	void reverse();					//	Reverses direction
	void checkPacman();				//	Checks for collision with Pacman
};
#endif // !GHOST
