#include "ghostHandler.h"
#include "Globals.h"
#include "objectLoader.hpp"
#include "Model.h"
#include "Audio.h"

void GhostHandler::Init()
{
	reset();	//	(re)Set data
}

void GhostHandler::update()
{
	checkPellets();	//	Check pellet count
	checkState();	//	Check state
	move();			//	Move
	draw();			//	Draw
}

void GhostHandler::reset()
{
	//	Reset objects
	ghosts[0] = Ghost(13, 14, ghostSpeed, 0);
	ghosts[0].active = true;
	ghosts[1] = Ghost(12, 17, ghostSpeed, 1);
	ghosts[1].active = true;
	ghosts[2] = Ghost(13, 17, ghostSpeed, 2);
	ghosts[3] = Ghost(14, 17, ghostSpeed, 3);
	//	Create Models
	for (int i = 0; i < 4; i++)
	{
		Model g(ShaderID, tGhosts[i], ghostObj,
			glm::vec3(0, 0, 0),
			glm::vec3(.9f, .9f, .9f),
			glm::vec3(0, 0, 0));
		ghosts[i].model.push_back(g);
	}
}

void GhostHandler::draw()
{
	for (Ghost g : ghosts) g.draw();	//	Draw models
}

void GhostHandler::move()
{
	if (!pacmanHandler.pacman.dying)
		for (int i = 0; i < 4; i++)
			ghosts[i].move();			//	Move
}

void GhostHandler::checkState()
{
	if (scareTimer > 0)	//	Scared
	{
		PlayBGM("pacman_theme", -1);	//	Play music
		scareTimer -= deltaTime;
	}
	else
	{
		PlayBGM("pacman_theme2", -1);	//	Play music

		for (int i = 0; i < 4; i++)
			ghosts[i].scared = false;

		//	Scatter / Chase mode
		phaseTimer += deltaTime;
		if (phaseCount < 4)
		{
			if (phaseTimer >= phaseTime)
			{
				phaseTimer = 0;
				if (phaseTime == 20)
				{
					if (phaseCount == 0 || phaseCount == 1)
						phaseTime = 7 - (level - 1);
					if (phaseCount == 2 || phaseCount == 3)
						phaseTime = 5 - (level - 1);
				}
				else
					phaseTime = 20;

				for (int i = 0; i < 4; i++)
					ghosts[i].changePhase();

				phaseCount++;
			}
		}
		else
		{
			//	Enforce chase mode
			for (int i = 0; i < 4; i++)
				ghosts[i].state = Chase;
		}
	}
}

void GhostHandler::changePhase(int id)
{
	ghosts[id].changePhase();	//	Change phase
}

void GhostHandler::scare(int id)
{
	if (!ghosts[id].dead && ghosts[id].active && !ghosts[id].scared)
	{
		ghosts[id].scared = true;								//	Scare ghost
		ghosts[id].model[0].sprite[0]->setTexture(tScaredBlue);	//	Set texture to scared
		ghosts[id].reverse();									//	Reverse direction
	}
}

void GhostHandler::checkPellets()
{
	if (pelletTotal - 30 >= pelletsActive)
		ghosts[2].active = true;			//	Activate ghost if 30 pellets eaten
	if (pelletTotal / 3 >= pelletsActive)
		ghosts[3].active = true;			//	Activate ghost if 1/3 pellets eaten
}
